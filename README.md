## Callback pattern example

### Description

An example of a callback pattern.

### Running the example

- Open a terminal and go to `service-lambda` folder.
- Run `$ npm start`, - the λ service will run on `localhost:3000`

- Open a terminal and go to `service-epsilon` folder.
- Run `$ npm start`, - the ε service will run on `localhost:3001`

- Send a request: `POST localhost:3000/lambda`,
  e.g. `$ curl -X POST "${LOCALHOST}:3000/lambda"`

- Inspect the logs for more information.


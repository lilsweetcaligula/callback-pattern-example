const app = require('./app.js')
const package_json = require('./package.json')

const PORT = 3001

app.listen(PORT, () => {
  console.log(`${package_json.name} is listening on port ${PORT}...`)
})

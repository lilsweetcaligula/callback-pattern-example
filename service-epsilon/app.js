const assert = require('assert')
const axios = require('axios')
const morgan = require('morgan')
const Express = require('express')

const app = Express()

app.use(morgan('combined'))

app.get('/epsilon', (req, res) => {
  const payload = { message: 'hic sunt leones' }

  if ('callback' in req.query) {
    const { callback } = req.query

    axios.get(callback, { params: payload })
      .then(_ => {
        return res.sendStatus(202)
      })
      .catch(err => {
        console.error(err)
        return res.sendStatus(500)
      })

    return
  }

  return res.json(payload)
})

module.exports = app

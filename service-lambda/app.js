const assert = require('assert')
const axios = require('axios')
const morgan = require('morgan')
const Express = require('express')

const app = Express()

app.use(morgan('combined'))

app.post('/lambda', (req, res) => {
  axios.get('http://127.0.0.1:3001/epsilon?callback=http://127.0.0.1:3000/call-me')
    .then(_ => {
      return res.sendStatus(200)
    })
    .catch(err => {
      console.error(err)
      return res.sendStatus(500)
    })
})

app.get('/call-me', (req, res) => {
  return res.sendStatus(204)
})

module.exports = app
